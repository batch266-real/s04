package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        System.out.println("This car is driven by " + myCar.getDriverName());

        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();

        System.out.println(myPet.getName() + " " + myPet.getBreed() + " " + myPet.getColor());

        myPet.setName("Hapi");
        myPet.setColor("Brown");
        myPet.setBreed("AsPin");
        myPet.speak();

        System.out.println("Name: " + myPet.getName() + "\nBreed: " + myPet.getBreed() + "\nColor: " + myPet.getColor());

        myCar.setName("Toyota");
        myCar.setBrand("Vios");
        myCar.setYearOfMake(2018);

        System.out.println("Car name: " + myCar.getName() + "\nCar Brand: " + myCar.getBrand() + "\nCar Driver: " + myCar.getDriverName());
        myCar.Drive();

        // Abstraction
        // is a process where all the logic and complexity are hidden from the user

        Person child = new Person();
        child.sleep();
        child.run();

        // Polymorphism
        // delivered from the greek word: poly means 'many' and morph means 'forms'
        StaticPoly myAddition = new StaticPoly();
        System.out.println(myAddition.addition(5, 6));
        System.out.println(myAddition.addition(5, 6, 10));
        System.out.println(myAddition.addition(5.5, 6.6));

    }
}
