package com.zuitt.example;

public class Dog extends Animal {
    private String breed;

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Dog(){
        super(); // Animal() constructor
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color); //Animal(String name, String color) >> constructor
        this.breed = breed;
    }

    // Getter
    public String getBreed() {
        return this.breed;
    }

    public void speak(){
        super.call(); // The call() method from Animal Class
        System.out.println("Woof Woof!");
    }
}
